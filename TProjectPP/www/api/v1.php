<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET");

include_once 'config.php';

$matches = array();
preg_match('/Bearer\s(\S+)/', $_SERVER['HTTP_AUTHORIZATION'], $matches);
if(isset($matches[1])){
  $token = $matches[1];
}
// Отлавливаем ошибки
function exceptions_error_handler($severity, $message, $filename, $lineno) {
    throw new ErrorException($message, 0, $severity, $filename, $lineno);
}
set_error_handler('exceptions_error_handler');

try{
    
if($token == HOME_TOKEN) {
    
    if(isset($_GET['method'])){
        switch($_GET['method']){
            case 'rates':         // Получение всех курсов с учетом комиссии
            
                if(isset($_GET['currency'])) {  // Курс по интересующей валюте
                    $currency = $_GET['currency']; 
                    foreach ($data as $field => $value){
                        if ($value->symbol == $currency) $result[$field] = round($value->last*$comm, 2);
                        } 
                } else 
                    foreach ($data as $field => $value){
                        $result[$field] = round($value->last*$comm, 2);
                    } 
                if(is_null($result)) $code = 404; // Валюта не найдена
                else {
                    asort($result, SORT_NUMERIC );  
                    $code = 200;
                    http_response_code($code);
                    echo json_encode(array("status" => "success", "code" => $code, "data" => $result), JSON_FORCE_OBJECT);
                }
            break;
            case 'convert':  // Запрос на обмен валюты
                if(isset($_POST['currency_from']) && isset($_POST['currency_to']) && isset($_POST['value'])){
                    if ($_POST['value'] >= $min_summ){ 
                        foreach ($data as $field => $value){        
                            if ($value->symbol == $_POST['currency_from']) $value_from = round($value->last*$comm, 2);
                            if ($value->symbol == $_POST['currency_to']) $value_to = round($value->last*$comm, 2);
                            if($_POST['currency_from'] == 'BTC') $value_from = 1;
                            if($_POST['currency_to'] == 'BTC') $value_to = 1;
                        } 
                        if ($value_from == '' or $value_to == '') {$code = 404;} // Валюта не найдена
                            else {
                                $converted_value = $comm*$_POST['value']*$value_to/$value_from;

                                // если сконвертированное значение слишком мало, то округляем до 0.01
                                $converted_value < 0.01 ? $converted_value = 0.01 : $converted_value = $converted_value ;

                                // $rate = round($comm*$value_to/$value_from, 2);
                                $rate = round($comm*$value_from, 2);
                                $result = ["currency_from" => $_POST['currency_from'],"currency_to" => $_POST['currency_to'], "value" => $_POST['value']*1.0, "converted_value" => $converted_value, "rate" => $rate];
                                $code = 200;
                                http_response_code($code);
                                echo json_encode(array("status" => "success", "code" => $code, "data" => $result), JSON_FORCE_OBJECT);
                            }
                    } else {$code = 400;}
                } else {$code = 400;}
            break;
            default:  $code = 400;
        } 
   } else{
        $code = 400;
    }
    if ($code != 200) {
        http_response_code($code);
        switch($code){
            case 400: $message = 'Bad Request'; break;
            case 404: $message = 'Not found'; break;
            default: $message = ' Unknown error';
        }
        echo json_encode(array("status" => "error", "code" => $code, "message" => $message ), JSON_UNESCAPED_UNICODE);
    }
} else{
    http_response_code(403);
    echo json_encode(array("status" => "error", "code" => 403, "message" => "Invalid token"), JSON_UNESCAPED_UNICODE);
}
} catch (Exception $e) {
    http_response_code(500);
    echo json_encode(array("status" => "error", "code" => 500, "message" => $e->getMessage()), JSON_UNESCAPED_UNICODE); 
}
