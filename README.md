# TestPP

Решение тестового задания

## Задание 1

Необходимо написать SQL запрос, который найдет и выведет всех читателей, возраста от 7 и до 17 лет, которые взяли только 2 книги и все книги одного и того же автора.

Решение:
```
select  u.id, 
		concat(u.first_name, ' ' ,u.last_name) name, 
        ub.author, 
		(SELECT group_concat(concat(`name`)) 
            FROM test_books tb, user_books b where tb.id = b.book_id and tb.author = ub.author and b.user_id=u.id) books
from (select user_id, author, count(test_books.id) cnt 
		from test_books, user_books, test_users 
		where test_books.id = user_books.book_id and test_users.id = user_books.user_id and age > 7 and age <17
		group by user_id, author 
		having count(test_books.id) = 2 )as ub, 
    test_users u
where ub.user_id = u.id; 
```

## Задание 2

Необходимо реализовать на  языке php JSON API для работы с курсами обмена валют для BTC. Реализовать с помощью Docker.

Решение в папке TProjectPP.

Скачать содержимое на сервер и выполниьт команду:
```
docker-compose up --build
```
Формат запроса :
```
<your_domain>:8000/api/v1?method=<method>
```
Сам код: https://gitlab.com/sdurmanov/testpp/-/blob/main/TProjectPP/www/api/v1.php
